from notedlib.database import queryall, execute, queryone
from notedlib.logging import logging

from notedlib.model.search import Search
from notedlib.model.search_list_result import SearchListResult

logger = logging.getLogger(__name__)


def create(search: Search) -> Search:
    logger.debug('Creating search %s' % search)

    execute('INSERT INTO searches (search_term, name) VALUES(?, ?)',
            (search.search_term, search.name))
    search.id = queryone('SELECT last_insert_rowid() AS id')['id']
    return search


def delete_by_id(id):
    logger.debug('Deleting search %s' % id)
    execute('DELETE FROM searches WHERE id = ?', (id,))


def list_searches():
    query = 'SELECT * FROM searches ORDER BY id'
    rows = queryall(query)
    result = SearchListResult()

    for row in rows:
        search = Search()
        search.populate(row)
        result.searches.append(search)

    return result
