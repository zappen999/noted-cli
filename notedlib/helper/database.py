def get_in_expr_placeholder(number_of_entries):
    return ','.join(list('?' * number_of_entries))
