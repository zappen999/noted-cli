class Plain():
    def __init__(self):
        pass

    def data(self, data):
        return '[DATA] ' + str(data)

    def info(self, message):
        return '[INFO] ' + message

    def error(self, message, trace):
        msg = '[ERROR] ' + message

        if trace:
            msg += '\n' + str(trace)

        return msg
