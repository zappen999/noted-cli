import json
from notedlib.model.base import Base


class JSON():
    def __init__(self):
        pass

    def data(self, data):
        raw_data = data

        # Handle primitive types
        if not isinstance(raw_data, (Base)):
            return self.pretty(raw_data)

        if getattr(data, 'to_raw'):
            raw_data = data.to_raw()

        return self.pretty(raw_data)

    def info(self, message: str):
        return self.pretty({
            'level': 'info',
            'message': message
        })

    def error(self, message: str, trace):
        return self.pretty({
            'level': 'error',
            'message': message,
            'trace': str(trace)
        })

    def pretty(self, data):
        return json.dumps(data, sort_keys=True, indent=4)
