from notedlib.model.base import Base


class Search(Base):
    def __init__(self, search_term: str = None, name: str = None):
        self.search_term = search_term
        self.name = name
        self.id = None

    def to_raw(self):
        return {
            'search_term': self.search_term,
            'name': self.name,
            'id': self.id,
        }
