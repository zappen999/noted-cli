from notedlib.model.base import Base


class SearchListResult(Base):
    def __init__(self):
        self.searches = []

    def to_raw(self):
        return {
            'searches': [search.to_raw() for search in self.searches],
        }
