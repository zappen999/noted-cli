import notedlib.storage as storage
import notedlib.database as database
import notedlib.migration as migration
from notedlib.tui.input import get_escape_delay, set_escape_delay
from .api import API


class Noted:
    def __init__(self):
        self.initial_escape_delay = None

    def __enter__(self):
        if self._is_first_use():
            self._install()

        self._handle_escape_delay()
        self._initialize_database_connection()
        return API()

    def _is_first_use(self):
        return not storage.directory_exists() or storage.is_empty()

    def _install(self):
        storage.ensure_directory_created()

    def _initialize_database_connection(self):
        database_file_path = storage.get_database_file_path()
        database.initialize_connection(database_file_path)
        migration.migrate()

    def _handle_escape_delay(self):
        self._backup_escape_delay()
        set_escape_delay(0)

    def _backup_escape_delay(self):
        self.initial_escape_delay = get_escape_delay()

    def _restore_escape_delay(self):
        set_escape_delay(self.initial_escape_delay)

    def __exit__(self, exception_type, exception_value, traceback):
        # TODO: Handle interrupts (CTRL-C)
        database.close_connection()
        # TODO: This is TUI-specific and should not be here.
        self._restore_escape_delay()
