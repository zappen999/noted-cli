#!/usr/bin/env python3
import curses
from notedlib.noted import Noted
from notedlib.tui.tui import TUIApp


def tui():
    with Noted() as api:
        tui = TUIApp(api)
        curses.wrapper(tui.main)


def cli():
    print('TODO!')


if __name__ == '__main__':
    tui()
