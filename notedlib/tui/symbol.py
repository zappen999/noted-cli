"""Unicode symbols"""
ARROW_UP = '↑'
ARROW_DOWN = '↓'

ANGLE_TOP = '┌'
ANGLE_BOT = '└'

SQUARE = '■'
