#!/usr/bin/env python3
from argparse import ArgumentParser
from notedlib.noted import Noted
import notedlib.output.output as output
import notedlib.helper.cli as cli_helper
from notedlib.exception import InvalidCommand

# TODO: Make this script runnable by setup.py console_scripts


def get_help_text():
    return '''noted <command> [<args>]

    NOTES:
    list-note        List notes
    create-note      Create a new note
    read-note        Get a note
    update-note      Update a note
    delete-note      Delete a note

    TAGS:
    create-tag       Create a new tag
    read-tag         Get a tag
    update-tag       Update a tag
    delete-tag       Delete a tag

    TAGGING:
    tag-note         Tag a note
    untag-note       Untag a note
'''


def get_parser():
    p = ArgumentParser(description='Noted CLI')
    p.add_argument('--format',
                   help='Output data format (plain, json)',
                   default='plain')
    sub_p = p.add_subparsers(dest='command')

    # Handle notes
    create_note_p = sub_p.add_parser('create-note')
    create_note_p.add_argument('--content',
                               required=True,
                               help='Note contents in markdown format',
                               type=str)
    create_note_p.add_argument('--tags',
                               help='Comma-separated list of tags')
    create_note_p.add_argument('--created-at',
                               help='Created date (Y-m-d H:M:S)',
                               type=cli_helper.validate_timestamp_string)
    create_note_p.add_argument('--updated-at',
                               help='Updated date (Y-m-d H:M:S)',
                               type=cli_helper.validate_timestamp_string)

    read_note_p = sub_p.add_parser('read-note')
    read_note_p.add_argument('--id',
                             required=True,
                             help='ID of the note to read',
                             type=int)

    update_note_p = sub_p.add_parser('update-note')
    update_note_p.add_argument('--id',
                               required=True,
                               help='ID of the note to update',
                               type=int)
    update_note_p.add_argument('--content',
                               required=True,
                               help='Note contents in markdown format',
                               type=str)

    delete_note_p = sub_p.add_parser('delete-note')
    delete_note_p.add_argument('--id',
                               required=True,
                               help='ID of the note to delete',
                               type=int)

    list_note_p = sub_p.add_parser('list-note')
    list_note_p.add_argument('--search',
                             help='Term to search for',
                             type=str)
    list_note_p.add_argument('--include-tags',
                             help='Comma-separated list of tags to match',
                             type=str)
    list_note_p.add_argument('--exclude-tags',
                             help='Comma-separated list of tags to exclude',
                             type=str)

    # Handle tags
    create_tag_p = sub_p.add_parser('create-tag')
    create_tag_p.add_argument('--name',
                              required=True,
                              help='Name of the tag',
                              type=str)

    read_tag_p = sub_p.add_parser('read-tag')
    read_tag_p.add_argument('--id',
                            required=True,
                            help='ID of the tag to read',
                            type=int)

    update_tag_p = sub_p.add_parser('update-tag')
    update_tag_p.add_argument('--id',
                              required=True,
                              help='ID of the tag to update',
                              type=int)
    update_tag_p.add_argument('--name',
                              required=True,
                              help='Tag name',
                              type=str)

    delete_tag_p = sub_p.add_parser('delete-tag')
    delete_tag_p.add_argument('--id',
                              required=True,
                              help='ID of the tag to delete',
                              type=int)

    sub_p.add_parser('list-tag')

    # Tagging
    tag_note_p = sub_p.add_parser('tag-note')
    tag_note_p.add_argument('--note-id',
                            required=True,
                            help='ID of the note to tag',
                            type=int)
    tag_note_p.add_argument('--tag-name',
                            required=True,
                            help='Tag name',
                            type=str)

    untag_note_p = sub_p.add_parser('untag-note')
    untag_note_p.add_argument('--note-id',
                              required=True,
                              help='ID of the note to untag',
                              type=int)
    untag_note_p.add_argument('--tag-name',
                              required=True,
                              help='Tag name to remove',
                              type=str)
    return p


def print_help_text():
    print(get_help_text())


def get_cli_args():
    cli_parser = get_parser()
    return vars(cli_parser.parse_args())  # Converts to dict


# Maps cli arguments to actual API calls
class CLICommandRunner:
    def __init__(self, api, args):
        self.api = api
        self.args = args

    def run_command(self):
        if not self.args.get('command'):
            raise InvalidCommand('You must specify a command')

        command_func_name = self.args.get('command').replace('-', '_')
        command_function = getattr(self, command_func_name)

        if not command_function:
            raise InvalidCommand('%s is not a valid command' %
                                 command_func_name)

        command_function()

    # Notes
    def create_note(self):
        tags = cli_helper.parse_tag_string(self.args.get('tags'))
        note = self.api.create_note(tags=tags,
                                    content=self.args.get('content'),
                                    created_at=self.args.get('created_at'),
                                    updated_at=self.args.get('updated_at'))
        output.data(note)

    def read_note(self):
        output.data(self.api.read_note(self.args.get('id')))

    def update_note(self):
        id = self.args.get('id')
        content = self.args.get('content')
        self.api.update_note(id, content=content)

    def delete_note(self):
        output.data(self.api.delete_note(self.args.get('id')))

    def list_note(self):
        term = self.args.get('search')
        include_tags = cli_helper.parse_tag_string(
            self.args.get('include_tags'))
        exclude_tags = cli_helper.parse_tag_string(
            self.args.get('exclude_tags'))

        list_notes_args = {
            'term': term,
            'include_tags': include_tags,
            'exclude_tags': exclude_tags,
        }

        output.data(self.api.list_notes(**list_notes_args))

    # Tags
    def create_tag(self):
        output.data(self.api.create_tag(name=self.args.get('name')))

    def read_tag(self):
        output.data(self.api.read_tag(self.args.get('id')))

    def update_tag(self):
        id = self.args.get('id')
        name = self.args.get('name')
        output.data(self.api.update_tag(id, name=name))

    def delete_tag(self):
        id = self.args.get('id')
        output.data(self.api.delete_tag(id))

    def list_tag(self):
        output.data(self.api.list_tags())

    def tag_note(self):
        output.data(self.api.tag_note(
            note_id=self.args.get('note_id'),
            tag_name=self.args.get('tag_name')
        ))

    def untag_note(self):
        output.data(self.api.untag_note(
            note_id=self.args.get('note_id'),
            tag_name=self.args.get('tag_name')
        ))


def run_cli():
    try:
        args = get_cli_args()
        output.set_adapter(args.get('format'))

        with Noted() as api:
            command_runner = CLICommandRunner(api, args)
            command_runner.run_command()
    except InvalidCommand as exc:
        return print_help_text()


if __name__ == '__main__':
    run_cli()
